class Config(object):
    DEBUG = True
    TESTING = False

    SECRET_KEY = "xashdugddewsdef3243dsfw43f3f34"

    

    # Flask-Mail SMTP server settings
    MAIL_SERVER = 'smtp.gmail.com'
    MAIL_PORT = 465
    MAIL_USE_SSL = True
    MAIL_USE_TLS = False
    MAIL_USERNAME = 'email@example.com'
    MAIL_PASSWORD = 'password'
    MAIL_DEFAULT_SENDER = '"MyApp" <noreply@example.com>'

    # Flask-User settings
    USER_APP_NAME = "Flask-User Basic App"      # Shown in and email templates and page footers
    USER_ENABLE_EMAIL = True        # Enable email authentication
    USER_ENABLE_USERNAME = False    # Disable username authentication
    USER_EMAIL_SENDER_NAME = USER_APP_NAME
    USER_EMAIL_SENDER_EMAIL = "noreply@example.com"


class ProductionConfig(Config):
    # Flask-SQLAlchemy settings
    SQLALCHEMY_DATABASE_URI = 'mysql://prertysd_mlm:{VL-oHEeU&O7@localhost/prertysd_mlm'    # File-based SQL database
    SQLALCHEMY_TRACK_MODIFICATIONS = False    # Avoids SQLAlchemy warning
    DEBUG = True

class DevelopmentConfig(Config):
    # Flask-SQLAlchemy settings
    SQLALCHEMY_DATABASE_URI = 'mysql://root:@localhost/mlmdemodata'    # File-based SQL database
    SQLALCHEMY_TRACK_MODIFICATIONS = False    # Avoids SQLAlchemy warning
    DEBUG = True

class TestingConfig(Config):
    TESTING = True