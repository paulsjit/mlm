from flask_wtf import FlaskForm
from flask_wtf.file import FileField, FileRequired, FileAllowed
from wtforms import StringField ,DateField, MultipleFileField, SelectField, IntegerField, PasswordField, SubmitField, BooleanField, DecimalField, TextAreaField, validators
from wtforms.validators import DataRequired, Length, Email, EqualTo, ValidationError


class ProductForm(FlaskForm):
    product_name = StringField('Product Name', validators=[DataRequired(), Length(min=2, max=50)])
    product_description = TextAreaField('Product Description', [validators.optional(), validators.length(max=200)])
    product_image = MultipleFileField('Photo(s) Upload')
    price = DecimalField('Price')
    product_gst = DecimalField('GST',validators=[DataRequired()])
    quantity=IntegerField('Quantity',validators=[DataRequired()])
    submit = SubmitField('Add Product')


class EmployeeForm(FlaskForm):
    first_name = StringField('First Name', validators=[DataRequired(), Length(min=2, max=20)])
    last_name = StringField('Last Name', validators=[DataRequired(), Length(min=2, max=20)])
    email = StringField('Email', validators=[DataRequired(), Email()])
    password = PasswordField('Set Password', validators=[DataRequired()])
    confirm_password = PasswordField('Confirm Set Password', validators=[DataRequired(), EqualTo('password')])
    username = StringField('Unique EmployeeID', validators=[DataRequired(), Length(min=2, max=20)], render_kw={'readonly': True})
    submit = SubmitField('Add New Eployee')  

class ApplicationForm(FlaskForm):
    first_name = StringField('First Name', validators=[DataRequired(), Length(min=2, max=20)])
    last_name = StringField('Last Name', validators=[DataRequired(), Length(min=2, max=20)])
    email = StringField('Email', validators=[DataRequired(), Email()])
    full_adress = TextAreaField('Full Address', [validators.optional(), validators.length(max=200)])
    gender = SelectField('Gender', choices=[('m', 'Male'), ('f', 'Female'), ('o', 'Other')])
    phone = StringField('Phone', validators=[DataRequired(), Length(10)])
    adhaar = IntegerField('Adhaar Number', validators=[DataRequired()])
    introducer = StringField('Introducer', validators=[DataRequired(), Length(min=2, max=20)])
    sponser = StringField('Sponser Name If Applicable', validators=[DataRequired(), Length(min=2, max=20)])
    care_of = StringField('S/O, W/O, C/O', validators=[DataRequired(), Length(min=2, max=20)])
    bank_name = StringField('Bank Name', validators=[DataRequired(), Length(min=2, max=20)])
    account_no = IntegerField('Account No', validators=[DataRequired()])
    dob = StringField('DOB - DDMMYYYY', validators=[DataRequired()])
    photo = FileField('Photo Less Than 50KB',validators=[FileRequired(), FileAllowed(['jpg', 'png'], 'Images only!')])
    ifsc_code = StringField('IFSC Code', validators=[DataRequired(), Length(min=2, max=20)])
    pan = StringField('PAN Card', validators=[DataRequired(), Length(min=8, max=12)])
    nominee = StringField('Nominee', validators=[DataRequired(), Length(min=8, max=12)])
    nominee_relation = StringField('Relation With Nominee', validators=[DataRequired(), Length(min=8, max=12)])
    username = StringField('Unique UserID', validators=[DataRequired(), Length(min=2, max=20)], render_kw={'readonly': True})
    submit = SubmitField('Add New Eployee') 
