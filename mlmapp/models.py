from mlmapp.appmain import db
from flask_user import UserMixin


class User(db.Model, UserMixin):
    __tablename__ = 'user'
    id = db.Column(db.Integer, primary_key=True)
    active = db.Column('is_active', db.Boolean(), nullable=False, server_default='1')

    # User authentication information. The collation='NOCASE' is required
    # to search case insensitively when USER_IFIND_MODE is 'nocase_collation'.
    email = db.Column(db.String(255), nullable=False, unique=True)
    email_confirmed_at = db.Column(db.DateTime())
    username = db.Column(db.String(50), nullable=False, unique=True)
    password = db.Column(db.String(255), nullable=False, server_default='')

    # User information
    first_name = db.Column(db.String(100), nullable=False, server_default='')
    last_name = db.Column(db.String(100), nullable=False, server_default='')
    gender = db.Column(db.String(20))
    #Sponser And introducer
    sponser = db.Column(db.String(50), nullable=False, server_default='Admin')
    introducer = db.Column(db.String(50), nullable=False, server_default='Admin')

    #Adress Section
    adhar = db.Column(db.String(120), nullable=False, server_default='000000000')
    dob = db.Column(db.String(120), nullable=False, server_default='000000000')
    careof = db.Column(db.String(120), nullable=False, server_default='Father Name')
    address = db.Column(db.String(120), nullable=False, server_default='Details Adress')
    phone = db.Column(db.String(120), nullable=False, server_default='9876543210')

    #Bank Details
    bank = db.Column(db.String(120), nullable=False, server_default='SBI')
    account = db.Column(db.String(120), nullable=False, server_default='0000000000000')
    ifsc = db.Column(db.String(120), nullable=False, server_default='SBIF0000')
    pan = db.Column(db.String(120), nullable=False, server_default='ABCDE1234F')
    nominee = db.Column(db.String(120), nullable=False, server_default='Admin')
    nominee_relation = db.Column(db.String(120), nullable=False, server_default='Father')
    photo = db.Column(db.LargeBinary)
    # Define the relationship to Role via UserRoles
    roles = db.relationship('Role', secondary='user_roles')


# Define the Role data-model
class Role(db.Model):
    __tablename__ = 'roles'
    id = db.Column(db.Integer(), primary_key=True)
    name = db.Column(db.String(50), unique=True)

# Define the UserRoles association table
class UserRoles(db.Model):
    __tablename__ = 'user_roles'
    id = db.Column(db.Integer(), primary_key=True)
    user_id = db.Column(db.Integer(), db.ForeignKey('user.id', ondelete='CASCADE'))
    role_id = db.Column(db.Integer(), db.ForeignKey('roles.id', ondelete='CASCADE'))


# Creating a s table for product details
class Product(db.Model):
    __tablename__ = 'product'
    product_id = db.Column(db.Integer, primary_key= True)
    product_name = db.Column(db.String(255), nullable= False)
    product_description = db.Column(db.String(255), nullable= False)
    product_image = db.Column(db.String(255), nullable= True)
    price  = db.Column(db.Integer(), nullable= False)
    product_gst = db.Column (db.Float(), nullable= False)
    quantity = db.Column(db.Integer(),nullable=False)



    