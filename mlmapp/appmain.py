from flask import Flask
from flask_sqlalchemy import SQLAlchemy

app = Flask(__name__)
app.config.from_object('mlmapp.config.DevelopmentConfig')
db = SQLAlchemy(app)



from mlmapp import routes