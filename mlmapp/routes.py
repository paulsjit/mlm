from mlmapp.appmain import app, db
from mlmapp.models import User, Role, UserRoles, Product
from flask import render_template, redirect, request
from flask_user import UserManager, login_required, current_user, roles_required
from mlmapp.forms import ApplicationForm, ProductForm, EmployeeForm
import datetime

user_manager = UserManager(app, db, User)

db.create_all()

if not User.query.filter(User.email == 'admin@example.com').first():
    user = User(
        email='admin@example.com',
        email_confirmed_at=datetime.datetime.utcnow(),
        password=user_manager.hash_password('Password1'),
        username = 'ARK-A9999',
        first_name = 'Satyajit',
        last_name = 'Paul',
    )
    user.roles.append(Role(name='Admin'))
    db.session.add(user)
    db.session.commit()

if not User.query.filter(User.email == 'employye@ark.com').first():
    user = User(
        email='employye@ark.com',
        email_confirmed_at=datetime.datetime.utcnow(),
        password=user_manager.hash_password('Password123'),
        username = 'ARK-E9999',
        first_name = 'Satyajit',
        last_name = 'Paul',
    )
    
    user.roles.append(Role(name='Employee'))
    db.session.add(user)
    db.session.commit()


if not User.query.filter(User.email == 'member@ark.com').first():
    user = User(
        email='member@ark.com',
        email_confirmed_at=datetime.datetime.utcnow(),
        password=user_manager.hash_password('Password123'),
        username = 'ARK-M9999',
        first_name = 'Satyajit',
        last_name = 'Paul',
    )    
    user.roles.append(Role(name='Member'))
    db.session.add(user)
    db.session.commit()


    # user_id = User.query.filter(User.email == 'ark@ark.com').first()
    # role_id = Role.query.filter(Role.name == 'Admin').first()
    # role_to_add = UserRoles(user_id = user_id.id, role_id = role_id.id)
    # db.session.add(role_to_add)
    # db.session.commit()

@app.route("/")
@login_required 
def home():
    return render_template("index.html")

@app.route("/new-product", methods=['POST','GET'])
@login_required
def new_product():
    form = ProductForm()
    if request.method == "POST":
        product_name = request.form.get('product_name')
        product_description = request.form.get('product_description')
        product_image= request.form.get('product_imag')
        price = request.form.get('price')
        product_gst= request.form.get('product_gst')
        quantity = request.form.get('quantity')
        db_product_obj = Product (product_name=product_name,product_description=product_description,product_image=product_image,price =price ,product_gst=product_gst,quantity=quantity)
        db.session.add(db_product_obj)
        db.session.commit()
        return redirect('/')
    return render_template("add-product.html", form = form)

@app.route("/all-member")
@login_required
def all_member():
    users = User.query.filter(User.roles.any(Role.id.in_([3]))).all()
    return render_template("all-member.html", users = users)

@app.route("/all-product")
def all_product():
    db_data= Product.query.all()
    return render_template("all-product.html", data = db_data)

@app.route("/genealogy")
@login_required
def genealogy():
    return render_template("genealogy.html")

@app.route("/all-employee")
@roles_required('Admin')
def all_employee():
    users = User.query.filter(User.roles.any(Role.id.in_([2]))).all()
    return render_template("all-employee.html", users = users)

@app.route("/add-employee" , methods=['GET', 'POST'])
@roles_required('Admin')
def add_employee():
    form = EmployeeForm()
    if form.validate_on_submit():
        fname = form.first_name.data
        lname = form.last_name.data
        email = form.email.data
        set_username = form.username.data
        set_password = form.password.data
        user = User(
            email=email,
            email_confirmed_at=datetime.datetime.utcnow(),
            password=user_manager.hash_password(set_password),
            username = set_username,
            first_name = fname,
            last_name = lname,
        )
        db.session.add(user)
        db.session.commit()
        user_id = User.query.filter(User.email == email).first()
        role_id = Role.query.filter(Role.name == 'Employee').first()
        role_to_add = UserRoles(user_id = user_id.id, role_id = role_id.id)
        db.session.add(role_to_add)
        db.session.commit()
        return redirect('/all-employee')
    
    last_id = User.query.order_by(User.id.desc()).first()
    uname = last_id.id + 1
    intPart = uname + 9999
    uname = "ARK-E" + str(intPart)
    return render_template("add-employee.html", form = form, uname = uname)


@app.route("/new-application" , methods=['GET', 'POST'])
@roles_required(['Admin', 'Employee'])
def new_application():
    form = ApplicationForm()
    if form.validate_on_submit():
        fname = form.first_name.data
        lname = form.last_name.data
        email = form.email.data
        set_username = form.username.data
        set_password = "Member@MLM1"
        careof = form.care_of.data
        full_adress = form.full_adress.data
        gender = form.gender.data
        phone = form.phone.data
        adhaar = form.adhaar.data
        introducer = form.introducer.data
        sponser = form.sponser.data
        bank_name = form.bank_name.data
        account_no = form.account_no.data
        ifsc_code = form.ifsc_code.data
        pan = form.pan.data
        nominee = form.nominee.data
        nominee_relation = form.nominee_relation.data
        dob = form.dob.data
        photo = form.photo.data.read()
        user = User(
            email=email,
            email_confirmed_at=datetime.datetime.utcnow(),
            password=user_manager.hash_password(set_password),
            username = set_username,
            first_name = fname,
            last_name = lname,
            careof = careof,
            gender = gender,    
            sponser = sponser,
            introducer = introducer,
            adhar = adhaar,
            dob = dob,
            address = full_adress,
            phone = phone,
            bank = bank_name,
            account = account_no,
            ifsc = ifsc_code,
            pan = pan,
            nominee = nominee,
            nominee_relation = nominee_relation,
            photo = photo,
        )
        db.session.add(user)
        db.session.commit()
        user_id = User.query.filter(User.email == email).first()
        role_id = Role.query.filter(Role.name == 'Member').first()
        role_to_add = UserRoles(user_id = user_id.id, role_id = role_id.id)
        db.session.add(role_to_add)
        db.session.commit()
        return redirect('/all-member')
    else:
        print('validation Error')
    last_id = User.query.order_by(User.id.desc()).first()
    uname = last_id.id + 1
    intPart = uname + 9999
    uname = "ARK-M" + str(intPart)
    return render_template("application.html", form = form, uname = uname)

@app.route("/product-delete/<string:num>")  
def deleteProduct(num):
    x = Product.query.filter_by(product_id= num).first()
    db.session.delete(x)
    db.session.commit()
    return redirect('/all-product')

@app.route("/status/<string:num>")  
def user_status(num):
    user = User.query.filter_by(id= num).first()
    if user.active:
        user.active = False
    else:
        user.activ = True
    db.session.commit()
    return redirect('/')